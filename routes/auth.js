/* GET authentication. */
var request= require('request');
var google = require('googleapis');
var plus = google.plus('v1');
var urlshortener = google.urlshortener('v1');
var OAuth2 = google.auth.OAuth2;
var readline = require('readline');
var querystring = require('querystring');
var CLIENT_ID = '581009286629-gs1fr305up4tfa6utp565rcikf48p4ip.apps.googleusercontent.com';
var CLIENT_SECRET = 'Q3zLJQ4gUzBrLymD47RhSs-A';
var REDIRECT_URL = 'http://localhost:3002/authenticated';
var params = { shortUrl: 'http://goo.gl/xKbRu3' };
var oauth2Client = new OAuth2(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);
var drive = google.drive({ version: 'v2', auth: oauth2Client });
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
var Spreadsheet = require('edit-google-spreadsheet');

// generate a url that asks permissions for Google+ and Google Calendar scopes
var mytoken;

function AuthController(){
	var self = this;
}
AuthController.prototype.index = function(req, res)
{
	
	 // get the long url of a shortened url
	urlshortener.url.get(params, function (err, response) {
		  console.log('Long url is', response.longUrl);
		});  
  var scopes = [
  'https://www.googleapis.com/auth/plus.me',
  'https://www.googleapis.com/auth/calendar',
  'https://www.googleapis.com/auth/userinfo.profile',
  'https://www.googleapis.com/auth/drive.install',
  'https://www.googleapis.com/auth/drive.file',
  'https://www.googleapis.com/auth/drive.readonly.metadata',
  'https://www.googleapis.com/auth/drive.readonly',
  'https://www.googleapis.com/auth/drive',
  'https://spreadsheets.google.com/feeds',
  ];

var google_url = oauth2Client.generateAuthUrl({
  access_type: 'offline', // 'online' (default) or 'offline' (gets refresh_token)
  scope: scopes // If you only need one scope you can pass it as string
    }); 
        console.log(google_url);
	    res.redirect(google_url);
}
AuthController.prototype.render = function(req, res)
{ 
	var ParamsWithValue = querystring.parse(require('url').parse(req.url).query);
	code=ParamsWithValue.code;
	console.log(code);
    function getAccessToken(oauth2Client, callback) {
    // generate consent page url
    var url = oauth2Client.generateAuthUrl({
     access_type: 'offline', // will return a refresh token
     scope: 'https://www.googleapis.com/auth/plus.me' // can be a space-delimited string or an array of scopes
     });
    // request access token
    oauth2Client.getToken(code, function(err, tokens) {
      // set tokens to the client
      // TODO: tokens should be set by OAuth2 client.
      mytoken=tokens.access_token;
      oauth2Client.setCredentials(tokens);
      callback();
    });
  
    }
    
    // retrieve an access token
	getAccessToken(oauth2Client, function() {
  		// retrieve user profile
    	plus.people.get({ userId: 'me', auth: oauth2Client }, function(err, profile) {
		     if (err) {
		      console.log('An error occured', err);
		       return;
		     }
		    console.log(profile.displayName, ':', profile.tagline);
		    
			  

     //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            // Check to see if user has an access_token first
				if (mytoken) {
                 console.log(mytoken);
				// URL endpoint and params needed to make the API call
				var url = 'https://www.googleapis.com/drive/v2/files';
				var params = {
					access_token: mytoken
				};

				// Send the API request
				request.get({url:url, qs:params}, function(err, resp, body) {
				// Handle any errors that may occur
				if (err) return console.error("Error occured: ", err);
					var list = JSON.parse(body);
				if (list.error) return console.error("Error returned from Google: ", list.error);

					// Generate output
					if (list.items && list.items.length > 0) {
						var file = ''
						  , iconLink = ''
						  , link = ''
						  ,object=''
						  ,test='https://ssl.gstatic.com/docs/doclist/images/icon_11_spreadsheet_list.png'
						  , output = '<h1>Your Google Drive</h1><ul>'
						  ,response=new Array();
						for(var i=0; i<list.items.length; i++) {
							file = list.items[i].title;
							iconLink = list.items[i].iconLink;
							link = list.items[i].alternateLink;
							object=list.items[i].title;
							 if(test==iconLink)
							 {
   							    response[i]=object;
								// output += '<li style="list-style-image:url('+iconLink+');"><a href="'+link+'" target="_blank">'+file+object+'</a></li>';
						     }
						}
						output += '</ul>';
						//Send output as response
  						res.render('list', { title: 'Express',list: response });
  						// res.writeHead(200, {'Content-Type': 'text/html'});
						// res.end(output);

					} else { // Alternate output if no files exist on Drive
						console.log('Could not retrieve any items.');

						res.writeHead(200, {'Content-Type': 'text/html'});
						res.end('<p>Your drive appears to be empty.</p>')
					}
				});
				// If no access_token was found, start the OAuth flow
				} else {
				console.log("Could not determine if user was authed. Redirecting to /");
				res.redirect('/');
				}
			

    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			
		});
  	});  

}
AuthController.prototype.getFile = function(req, res)
{   
	var result;

    // Spreadsheet.load({
			 //    debug: true,
			 //    spreadsheetName: 'work',
			 //     accessToken : {
			 //       type: 'Bearer',
			 //       token: mytoken
			 //     }
			 //  }, function sheetReady(err, spreadsheet) {
			 //    if(err) throw err;
			 //    console.log(spreadsheet);		
			 //    spreadsheet.metadata(function(err, metadata){
			 //      if(err) throw err;
			 //      console.log(metadata);
			 //      // { title: 'Sheet3', rowCount: '100', colCount: '20', updated: [Date] }
			 //    });
			 //   });   
    
    Spreadsheet.load({
			    debug: true,
			    spreadsheetName: 'work',
			     worksheetName: 'x2',
			     accessToken : {
			       type: 'Bearer',
			       token: mytoken
			     }
			  }, function sheetReady(err, spreadsheet) {
			  	 if(err){console.log(err);}
			  	 console.log(JSON.stringify(spreadsheet.raw));
			  	  console.log(JSON.stringify(spreadsheet)); 
			    //    spreadsheet.receive(function(err, rows, info) {
			    //   if(err) throw err;
			    //   console.log("Found rows:", rows);
			    //   result=rows; 
			    // });
			    // spreadsheet.metadata(function(err, metadata){
			    //   if(err) throw err;
			    //   console.log(metadata);
			    //   // { title: 'Sheet3', rowCount: '100', colCount: '20', updated: [Date] }
			    // });

			  });

  res.send(result);

}
module.exports = new AuthController();
