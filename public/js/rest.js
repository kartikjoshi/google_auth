/*
	REST.js

	Responsible for all server-side ajax calls

*/

function AjaxService(){
	var self = this;
	self.url = "";
}


/* Get Users for the enterprise */
AjaxService.prototype.getAuth = function(callback){
	var url = this.url + "/auth";
	$.ajax({
        async:true,
        url: url,
        type: "post",
        context: document.body,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function(data) {
            callback(data);
            console.log("back");
    	}
    });
}

var ajaxService = new AjaxService();